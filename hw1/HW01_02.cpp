#include "HW01_02.h"

//use namespace::struct::function to implement function in struct
double MyGeometry::Point::Length() { return sqrt(x * x + y * y); };
string MyGeometry::Point::CoutPoint()
{
	return "(" + to_string(x) + "," + to_string(y) + ")";
};
struct MyGeometry::Point* MyGeometry::LoadPoint(const char* filename, unsigned int& nPoint)
{
	ifstream inFile;
	struct Point* p = NULL;

	inFile.open(filename, ios::in);
	//Check file open success
	if (!inFile)
	{
		cerr << "Fail to open data\n";
		exit(1);
	}

	for (nPoint = 0; ; ++nPoint)
	{
		float x = 0,
			y = 0;
		if (!(inFile >> x >> y))
		{
			cout << "error data input\n";
			exit(2);
		}

		//read the end character of file or not
		if (inFile.eof())
			break;

		//Frist time read need to create the first space for pointer
		//After frist read we need to resize the dynamic memory size
		//Use malloc and realloc (c instruct) can easy to do this
		if (nPoint == 0)
		{
			p = (struct Point*)malloc(sizeof(struct Point) * 1);
			p->x = x;
			p->y = y;
		}
		else
		{
			p = (struct Point*)realloc(p, sizeof(struct Point) * (nPoint + 1));
			p[nPoint].x = x;
			p[nPoint].y = y;
		}
	}
	return p;
};
struct MyGeometry::Point* MyGeometry::MinDistance(struct Point *p, const int & nPoint)
{
	//Put point to calculate x + y - 5, the answer is the error.
	//If error is zero,the this point is on x + y = 5.
	//So we just need to find the minmum error.
	int err;
	struct Point* minPoint = NULL;
	for (int i = 0; i < nPoint; ++i)
	{
		if (i == 0)
		{
			err = abs(p[i].x + p[i].y - 5);
			minPoint = p;
		}
		else
		{
			if (err > abs(p[i].x + p[i].y - 5))
			{
				err = abs(p[i].x + p[i].y - 5);
				minPoint = p + i;
			}
		}
	}
	return minPoint;
};
void MyGeometry::SortbyLength(struct Point *p, const int & nPoint)
{
	float temp[2];
	for (int i = 0; i < nPoint; ++i)
	{
		for (int j = i + 1; j < nPoint; ++j)
		{
			if (p[i].Length() > p[j].Length())
			{
				temp[0] = p[i].x;
				temp[1] = p[i].y;
				p[i].x = p[j].x;
				p[i].y = p[j].y;
				p[j].x = temp[0];
				p[j].y = temp[0];
			}
		}
	}
};
void MyGeometry::Report(const char* fileName)
{
	unsigned int nPoint = 0;
	struct Point *p;
	p = LoadPoint(fileName, nPoint);
	SortbyLength(p, nPoint);
	cout << "物件導向程式設計第一次作業\n"
		 << "E14062034 張少鈞\n"
		 << "點數目 : " << nPoint
		<< "\n最接近 x + y = 5 的點 : " << MinDistance(p, nPoint)->CoutPoint() << endl
	    << "與原點距離最遠的點 : " << p[nPoint - 1].CoutPoint() << " 距離為 : " << p[nPoint - 1].Length() << endl;
	free(p);
};
