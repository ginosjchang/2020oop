// E14062034 張少鈞 HW01_01

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <cfloat> // use DBL_MAX DBL_MIN
#include <math.h>

using namespace std;

template <typename T>
T findMax(T*, int);

char* findMax(char**, int);

int main(int argc, char* argv[]) {
	srand(time(NULL));

	short *shpt = new short[8];
	cout << "short array is : ";
	// Randam create short number and output on screen;
	for (int i = 0; i < 8; ++i)
	{
		*(shpt + i) = rand() % (SHRT_MAX - SHRT_MIN) - SHRT_MIN;
		cout << *(shpt + i) << " ";
	}

	cout << "\nMax of short array is : " << findMax(shpt, 8) << endl;

	double *dbpt = new double[8];
	cout << "\n\ndouble array is : ";
	for (int i = 0; i < 8; ++i)
	{
		*(dbpt + i) = DBL_MIN + ((double)rand() / RAND_MAX)* (DBL_MAX - DBL_MIN);
		cout << *(dbpt + i) << " ";
	}

	cout << "\nMax of short arry is : " << findMax(dbpt, 8) << endl;

	cout << "\n\nchar array is : " << endl;
	char **chpt = new char*[8];
	for (int i = 0; i < 8; ++i)
	{
		//Determind the size of chpt[i]
		int size = rand() % 10 + 1;
		*(chpt + i) = new char[size];
		cout << "char address : " << (void *) *(chpt + i) << " char : ";
		//Create 1-D char array and put \0 in the end of array
		for (int j = 0; j < size; ++j)
		{
			if (j == size)
				*(*(chpt + i) + j) = '\0';
			else
				*(*(chpt + i) + j) = rand() % (126 - 21) + 21;

			cout << *(*(chpt + i) + j);
		}
		cout << endl;
	}

	char* maxCharArray = findMax(chpt, 8);
	cout << "\nMax char address is : " << (void *)maxCharArray << endl;

	//Delete the dynamic memory alloc
	delete[] shpt;
	delete[] dbpt;
	for (int i = 0; i < 8; ++i)
	{
		delete[] chpt[i];
	}
	delete[] chpt;
	return 0;
}

template <typename T>
T findMax(T* array, int size)
{
	T max = *array;
	for (int i = 1; i < size; ++i)
	{
		if (*(array + i) > max)
			max = *(array + i);
	}

	return max;
}

char* findMax(char **array, int size)
{
	char* max = NULL;
	int maxLength = 0;
	for (int i = 0; i < size; ++i)
	{
		int length = 0;
		for (int j = 0; *(*(array + i) + j) != '\0'; ++length, ++j);
		if (length > maxLength)
		{
			max = *(array + i);
			maxLength = length;
		}
	}
	return max;
}

