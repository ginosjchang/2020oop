#ifndef HW01_02_H
#define HW01_02_H

#include <iostream>
#include <string>
#include <math.h>
#include <fstream>

using namespace std;

namespace MyGeometry
{
	struct Point
	{
		float x;
		float y;
		double Length();
		string CoutPoint();
	};

	struct Point* LoadPoint(const char* filename, unsigned int& nPoint);
	struct Point* MinDistance(struct Point *p, const int & nPoint);
	void SortbyLength(struct Point *p, const int & nPoint);
	void Report(const char* fileName);
}

#endif
