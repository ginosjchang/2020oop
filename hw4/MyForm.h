#pragma once
/********E14062034 Chang Shoa-Chun 2020 OOP HW4********/
namespace HW04 {
#include <Windows.h>
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

#define PICTURENUMBER 6

	/// <summary>
	/// MyForm 的摘要
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	private:
		//PictureBox ArrayList
		ArrayList^ picboxlist = gcnew ArrayList();
		//Picture ArrayList
		ArrayList^ bitmapList = gcnew ArrayList();
		//Record index of picture in PictureBox
		array<int>^ picIndex;
		//Record score for each player
		array<int>^ score = gcnew array<int>(2);
		//Record the opened PictureBox in each ture
		array<int>^ open = gcnew array<int>(2);
		
		int showIndex;
		DateTime start;
		bool click_enable,
			player;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Label^  time_Label;
	private: System::Windows::Forms::Timer^  check_timer;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;

	private: System::Windows::Forms::ToolStripStatusLabel^  player_Label;
	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel2;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Label^  player2Score_Label;
	private: System::Windows::Forms::Label^  player1Score_Label;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Timer^  showtimer;
	private: System::Windows::Forms::ToolStripStatusLabel^  mouse_Label;
			 
	public:
		MyForm(void)
		{
			InitializeComponent();
			//Load pictures
			picIndex = gcnew array<int>(PICTURENUMBER * 2);
			try {
				bitmapList->Add(gcnew Bitmap("Eevee.jpg"));
				bitmapList->Add(gcnew Bitmap("Pyro.jpg"));
				bitmapList->Add(gcnew Bitmap("Rainer.jpg"));
				bitmapList->Add(gcnew Bitmap("Sakura.jpg"));
				bitmapList->Add(gcnew Bitmap("Sparky.jpg"));
				bitmapList->Add(gcnew Bitmap("Tamao.jpg"));
			}
			catch (const char *s)
			{
				MessageBox::Show("picture not found");
				Environment::Exit(100);
			}

			for (int i = 0; i < PICTURENUMBER; ++i)
			{
				picIndex[i * 2] = i;
				picIndex[i * 2 + 1] = i;
			}
			//Put PictureBox in PictureBox ArrayList
			picboxlist->Add(pictureBox1);
			picboxlist->Add(pictureBox2);
			picboxlist->Add(pictureBox3);
			picboxlist->Add(pictureBox4);
			picboxlist->Add(pictureBox5);
			picboxlist->Add(pictureBox6);
			picboxlist->Add(pictureBox7);
			picboxlist->Add(pictureBox8);
			picboxlist->Add(pictureBox9);
			picboxlist->Add(pictureBox10);
			picboxlist->Add(pictureBox11);
			picboxlist->Add(pictureBox12);
			//Add Click event and mouse enter event
			for (int i = 0; i < picboxlist->Count; ++i)
			{
				((PictureBox^)picboxlist[i])->Click += gcnew EventHandler(this, &MyForm::pictureBox_Click);
				((PictureBox^)picboxlist[i])->MouseEnter += gcnew EventHandler(this, &MyForm::pictureBox_MouseEnter);
			}

			newGame();
		}

	protected:
		/// 清除任何使用中的資源。
		~MyForm()
		{
			delete[] score;
			delete[] picIndex;
			delete[] open;
			delete picboxlist;
			delete bitmapList;
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::PictureBox^  pictureBox5;
	private: System::Windows::Forms::PictureBox^  pictureBox6;
	private: System::Windows::Forms::PictureBox^  pictureBox7;
	private: System::Windows::Forms::PictureBox^  pictureBox8;
	private: System::Windows::Forms::PictureBox^  pictureBox9;
	private: System::Windows::Forms::PictureBox^  pictureBox10;
	private: System::Windows::Forms::PictureBox^  pictureBox11;
	private: System::Windows::Forms::PictureBox^  pictureBox12;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::RadioButton^  player2_radiusBtn;
	private: System::Windows::Forms::RadioButton^  player1_radiusBtn;
	private: System::Windows::Forms::Button^  control_Btn;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// 設計工具所需的變數。


#pragma region Windows Form Designer generated code
		/// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
		/// 這個方法的內容。
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox7 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox8 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox9 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox10 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox11 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox12 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->player2_radiusBtn = (gcnew System::Windows::Forms::RadioButton());
			this->player1_radiusBtn = (gcnew System::Windows::Forms::RadioButton());
			this->control_Btn = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->time_Label = (gcnew System::Windows::Forms::Label());
			this->check_timer = (gcnew System::Windows::Forms::Timer(this->components));
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->player_Label = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->toolStripStatusLabel2 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->mouse_Label = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->player2Score_Label = (gcnew System::Windows::Forms::Label());
			this->player1Score_Label = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->showtimer = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox10))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox11))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox12))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox1->Location = System::Drawing::Point(13, 22);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(180, 180);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox2->Location = System::Drawing::Point(193, 22);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(180, 180);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 1;
			this->pictureBox2->TabStop = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox3->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox3->Location = System::Drawing::Point(373, 22);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(180, 180);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox3->TabIndex = 2;
			this->pictureBox3->TabStop = false;
			// 
			// pictureBox4
			// 
			this->pictureBox4->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox4->Location = System::Drawing::Point(553, 22);
			this->pictureBox4->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(180, 180);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox4->TabIndex = 3;
			this->pictureBox4->TabStop = false;
			// 
			// pictureBox5
			// 
			this->pictureBox5->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox5->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox5->Location = System::Drawing::Point(13, 202);
			this->pictureBox5->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(180, 180);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox5->TabIndex = 4;
			this->pictureBox5->TabStop = false;
			// 
			// pictureBox6
			// 
			this->pictureBox6->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox6->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox6->Location = System::Drawing::Point(193, 202);
			this->pictureBox6->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(180, 180);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox6->TabIndex = 5;
			this->pictureBox6->TabStop = false;
			// 
			// pictureBox7
			// 
			this->pictureBox7->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox7->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox7->Location = System::Drawing::Point(373, 202);
			this->pictureBox7->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox7->Name = L"pictureBox7";
			this->pictureBox7->Size = System::Drawing::Size(180, 180);
			this->pictureBox7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox7->TabIndex = 6;
			this->pictureBox7->TabStop = false;
			// 
			// pictureBox8
			// 
			this->pictureBox8->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox8->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox8->Location = System::Drawing::Point(553, 202);
			this->pictureBox8->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox8->Name = L"pictureBox8";
			this->pictureBox8->Size = System::Drawing::Size(180, 180);
			this->pictureBox8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox8->TabIndex = 7;
			this->pictureBox8->TabStop = false;
			// 
			// pictureBox9
			// 
			this->pictureBox9->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox9->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox9->Location = System::Drawing::Point(13, 382);
			this->pictureBox9->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox9->Name = L"pictureBox9";
			this->pictureBox9->Size = System::Drawing::Size(180, 180);
			this->pictureBox9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox9->TabIndex = 8;
			this->pictureBox9->TabStop = false;
			// 
			// pictureBox10
			// 
			this->pictureBox10->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox10->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox10->Location = System::Drawing::Point(193, 382);
			this->pictureBox10->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox10->Name = L"pictureBox10";
			this->pictureBox10->Size = System::Drawing::Size(180, 180);
			this->pictureBox10->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox10->TabIndex = 9;
			this->pictureBox10->TabStop = false;
			// 
			// pictureBox11
			// 
			this->pictureBox11->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox11->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox11->Location = System::Drawing::Point(373, 382);
			this->pictureBox11->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox11->Name = L"pictureBox11";
			this->pictureBox11->Size = System::Drawing::Size(180, 180);
			this->pictureBox11->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox11->TabIndex = 10;
			this->pictureBox11->TabStop = false;
			// 
			// pictureBox12
			// 
			this->pictureBox12->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox12->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox12->Location = System::Drawing::Point(553, 382);
			this->pictureBox12->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->pictureBox12->Name = L"pictureBox12";
			this->pictureBox12->Size = System::Drawing::Size(180, 180);
			this->pictureBox12->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox12->TabIndex = 11;
			this->pictureBox12->TabStop = false;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->player2_radiusBtn);
			this->groupBox1->Controls->Add(this->player1_radiusBtn);
			this->groupBox1->Location = System::Drawing::Point(763, 22);
			this->groupBox1->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->groupBox1->Size = System::Drawing::Size(166, 107);
			this->groupBox1->TabIndex = 12;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"先手";
			// 
			// player2_radiusBtn
			// 
			this->player2_radiusBtn->AutoSize = true;
			this->player2_radiusBtn->Location = System::Drawing::Point(19, 73);
			this->player2_radiusBtn->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->player2_radiusBtn->Name = L"player2_radiusBtn";
			this->player2_radiusBtn->Size = System::Drawing::Size(67, 18);
			this->player2_radiusBtn->TabIndex = 1;
			this->player2_radiusBtn->TabStop = true;
			this->player2_radiusBtn->Text = L"玩家二";
			this->player2_radiusBtn->UseVisualStyleBackColor = true;
			this->player2_radiusBtn->CheckedChanged += gcnew System::EventHandler(this, &MyForm::player2_radiusBtn_CheckedChanged);
			// 
			// player1_radiusBtn
			// 
			this->player1_radiusBtn->AutoSize = true;
			this->player1_radiusBtn->Location = System::Drawing::Point(19, 36);
			this->player1_radiusBtn->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->player1_radiusBtn->Name = L"player1_radiusBtn";
			this->player1_radiusBtn->Size = System::Drawing::Size(67, 18);
			this->player1_radiusBtn->TabIndex = 0;
			this->player1_radiusBtn->TabStop = true;
			this->player1_radiusBtn->Text = L"玩家一";
			this->player1_radiusBtn->UseVisualStyleBackColor = true;
			this->player1_radiusBtn->CheckedChanged += gcnew System::EventHandler(this, &MyForm::player1_radiusBtn_CheckedChanged);
			// 
			// control_Btn
			// 
			this->control_Btn->Location = System::Drawing::Point(763, 456);
			this->control_Btn->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->control_Btn->Name = L"control_Btn";
			this->control_Btn->Size = System::Drawing::Size(180, 50);
			this->control_Btn->TabIndex = 13;
			this->control_Btn->Text = L"Start";
			this->control_Btn->UseVisualStyleBackColor = true;
			this->control_Btn->Click += gcnew System::EventHandler(this, &MyForm::control_Btn_Click);
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// time_Label
			// 
			this->time_Label->AutoSize = true;
			this->time_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->time_Label->Font = (gcnew System::Drawing::Font(L"標楷體", 25, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(136)));
			this->time_Label->Location = System::Drawing::Point(741, 522);
			this->time_Label->Name = L"time_Label";
			this->time_Label->Size = System::Drawing::Size(221, 36);
			this->time_Label->TabIndex = 14;
			this->time_Label->Text = L"00:00:00.000";
			// 
			// check_timer
			// 
			this->check_timer->Interval = 1000;
			this->check_timer->Tick += gcnew System::EventHandler(this, &MyForm::check_timer_Tick);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->player_Label, this->toolStripStatusLabel2,
					this->mouse_Label
			});
			this->statusStrip1->Location = System::Drawing::Point(0, 575);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(990, 22);
			this->statusStrip1->TabIndex = 15;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// player_Label
			// 
			this->player_Label->Name = L"player_Label";
			this->player_Label->Size = System::Drawing::Size(78, 17);
			this->player_Label->Text = L"player_Label";
			// 
			// toolStripStatusLabel2
			// 
			this->toolStripStatusLabel2->Name = L"toolStripStatusLabel2";
			this->toolStripStatusLabel2->Size = System::Drawing::Size(79, 17);
			this->toolStripStatusLabel2->Text = L"滑鼠目前位置";
			// 
			// mouse_Label
			// 
			this->mouse_Label->Name = L"mouse_Label";
			this->mouse_Label->Size = System::Drawing::Size(18, 17);
			this->mouse_Label->Text = L"(,)";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->player2Score_Label);
			this->groupBox2->Controls->Add(this->player1Score_Label);
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->label1);
			this->groupBox2->Location = System::Drawing::Point(763, 224);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(166, 132);
			this->groupBox2->TabIndex = 16;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"分數";
			// 
			// player2Score_Label
			// 
			this->player2Score_Label->AutoSize = true;
			this->player2Score_Label->Location = System::Drawing::Point(92, 91);
			this->player2Score_Label->Name = L"player2Score_Label";
			this->player2Score_Label->Size = System::Drawing::Size(14, 14);
			this->player2Score_Label->TabIndex = 3;
			this->player2Score_Label->Text = L"0";
			// 
			// player1Score_Label
			// 
			this->player1Score_Label->AutoSize = true;
			this->player1Score_Label->Location = System::Drawing::Point(89, 49);
			this->player1Score_Label->Name = L"player1Score_Label";
			this->player1Score_Label->Size = System::Drawing::Size(14, 14);
			this->player1Score_Label->TabIndex = 2;
			this->player1Score_Label->Text = L"0";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(22, 90);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(49, 14);
			this->label2->TabIndex = 1;
			this->label2->Text = L"玩家二";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(19, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(49, 14);
			this->label1->TabIndex = 0;
			this->label1->Text = L"玩家一";
			// 
			// showtimer
			// 
			this->showtimer->Interval = 500;
			this->showtimer->Tick += gcnew System::EventHandler(this, &MyForm::showtimer_Tick);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->ClientSize = System::Drawing::Size(990, 597);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->time_Label);
			this->Controls->Add(this->control_Btn);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->pictureBox12);
			this->Controls->Add(this->pictureBox11);
			this->Controls->Add(this->pictureBox10);
			this->Controls->Add(this->pictureBox9);
			this->Controls->Add(this->pictureBox8);
			this->Controls->Add(this->pictureBox7);
			this->Controls->Add(this->pictureBox6);
			this->Controls->Add(this->pictureBox5);
			this->Controls->Add(this->pictureBox4);
			this->Controls->Add(this->pictureBox3);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Font = (gcnew System::Drawing::Font(L"標楷體", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(136)));
			this->Margin = System::Windows::Forms::Padding(4, 3, 4, 3);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox10))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox11))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox12))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: void check()
	{
		//First layer of protection
		//check the index of PictureBox ArrayList is in valid range.
		if (open[0] < 0 || open[0] > PICTURENUMBER * 2) return;
		if (open[1] < 0 || open[1] > PICTURENUMBER * 2) return;

		//Check bitmap index of two PictureBox
		if (picIndex[open[0]] != picIndex[open[1]])
		{
			//Two PictureBoxs contain different bitmap.
			//Hide pictures form PictureBoxs
			((PictureBox^)picboxlist[open[0]])->Image = nullptr;
			((PictureBox^)picboxlist[open[1]])->Image = nullptr;
			//Next player
			player = !player;
			if (player)
				player_Label->Text = "玩家一";
			else
				player_Label->Text = "玩家二";
		}
		else
		{
			//Record Score
			if (player)
			{
				++score[0];
				player1Score_Label->Text = score[0].ToString();
			}
			else
			{
				++score[1];
				player2Score_Label->Text = score[1].ToString();
			}
			//Check this game is end or not.
			if (score[0] + score[1] == PICTURENUMBER)
			{
				timer1->Stop();
				check_timer->Stop();
				if (score[0] > score[1])
					MessageBox::Show("獲勝者是玩家一", "遊戲結束");
				else if (score[0] == score[1])
					MessageBox::Show("平手", "遊戲結束");
				else
					MessageBox::Show("獲勝者是玩家二", "遊戲結束");

				//Click Stop button to rest.
				control_Btn->PerformClick();
			}
		}
		//Clear record of this turn.
		open[0] = -1;
		open[1] = -1;
	}

	private: System::Void control_Btn_Click(System::Object^  sender, System::EventArgs^  e) {
		Button^ btn = safe_cast<Button^>(sender);
		if (btn->Text == "Start")
		{
			groupBox1->Enabled = false;
			//After show each picture in PictureBox, showIndex isn't equal to 0.
			/*First Click*/
			if(showIndex == 0) //First start click 
				if (MessageBox::Show("是否啟動翻牌功能?", "", MessageBoxButtons::OKCancel) == System::Windows::Forms::DialogResult::OK)
				{
					//Click OK to start show each picture in PictureBox.
					showtimer->Start();
					control_Btn->Enabled = false;
					return;
				}
			/*Second Click*/
			click_enable = true;
			start = DateTime::Now;
			btn->Text = "Stop";
			timer1->Start();
		}
		else //button text is stop. Create new game.
			newGame();
	}
	
	private: System::Void pictureBox_Click(System::Object^  sender, System::EventArgs^  e) {
		// can be clicked or not
		if (!click_enable) return;
		//Turn sender form Object to PictureBox
		PictureBox^ pic = safe_cast<PictureBox^>(sender);
		//if Image isn't a nullptr, this PictureBox is opened.
		if (pic->Image != nullptr) return;

		//Get this PictureBox index in PictureBoxArrayList
		int index = picboxlist->IndexOf(pic);
		//Using index of PictureBox to get Bitmap form BitmapArraList 
		pic->Image = (Bitmap^)bitmapList[picIndex[index]];
		//if open[0] = -1 ,this click is first click in this turn.
		if (open[0] == -1)
			open[0] = index;
		else
		{
			//Second click in this turn, we need to cheek two Bitmap in those PictureBox is same or not.
			open[1] = index;
			//can't click any PictureBox when checking.(To skip error)
			click_enable = false;
			//Use timer to let picture can be showed on PictureBox
			check_timer->Start();
		}
	}
	
	private: void newGame()
	{
		//Score rest
		score[0] = 0;
		score[1] = 0;
		player1Score_Label->Text = score[0].ToString();
		player2Score_Label->Text = score[1].ToString();

		//Timer Stop and rest time
		timer1->Stop();
		check_timer->Stop();
		showtimer->Stop();
		time_Label->Text = "00:00:00.000";

		//Set text of button
		control_Btn->Text = "Start";

		//Open first hand selection and set first hand player is player1
		groupBox1->Enabled = true;
		player_Label->Text = "玩家一";
		player = true;

		//Rest turn recorder
		open[0] = -1;
		open[1] = -1;

		
		/*PictureBox Rest*/
		//Set PictureBox can't be clicked.
		click_enable = false;
		//Set each PictureBox not to show picture. 
		for (int i = 0; i < picboxlist->Count; ++i)
			((PictureBox^)picboxlist[i])->Image = nullptr;

		//Reandom set bitmaps' order
		Random random;
		for (int i = 0; i < 50; ++i)
		{
			int first = random.Next(0, 12);
			int second = random.Next(0, 12);
			int temp = picIndex[first];
			picIndex[first] = picIndex[second];
			picIndex[second] = temp;
		}

		//Rest showIndex
		showIndex = 0;
	}

	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		//Count time
		TimeSpan diff = DateTime::Now - start;
		time_Label->Text = diff.Hours.ToString("00") + ":" + diff.Minutes.ToString("00") + ":" + diff.Seconds.ToString("00") + "." + diff.Milliseconds;
	}

	private: System::Void check_timer_Tick(System::Object^  sender, System::EventArgs^  e) {
		check();
		check_timer->Stop();
		click_enable = true;
	}

	//When mouse enter PictureBox, trigger this function
	private: System::Void pictureBox_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
		PictureBox^ pic = safe_cast<PictureBox^>(sender);
		//Get index of this PictureBox in PictureBox ArrayList.
		int index = picboxlist->IndexOf(pic);
		//Turn index to position
		mouse_Label->Text = "( " + (index / 4 + 1) + " , " + (index % 4 + 1) + " )";
	}

	private: System::Void player1_radiusBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		//When this radius button be clicked, set first hand player is player1
		player_Label->Text = "玩家一";
		player = true;
	}

	private: System::Void player2_radiusBtn_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		//When this radius button be clicked, set first hand player is player2
		player_Label->Text = "玩家二";
		player = false;
	}

	private: System::Void showtimer_Tick(System::Object^  sender, System::EventArgs^  e) {
		if (showIndex >= PICTURENUMBER * 2)
		{
			//End of show
			((PictureBox^)picboxlist[showIndex - 1])->Image = nullptr;
			showtimer->Stop();
			control_Btn->Enabled = true;
			//Click start button to start
			control_Btn->PerformClick();
		}
		else
		{
			//Show the picture in this PictureBox.
			((PictureBox^)picboxlist[showIndex])->Image = (Bitmap^)bitmapList[picIndex[showIndex]];
			//Close the picture in previous PictureBox.
			if (showIndex != 0)
				((PictureBox^)picboxlist[showIndex - 1])->Image = nullptr;
			//go to next index
			++showIndex;
		}
	}
};
}
