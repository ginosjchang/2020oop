#include "HW02.h"

//Initial data with zero
Point::Point() { x = 0; y = 0; };

//Initial data with input x, y
Point::Point(float x, float y) { Set_data(x, y); };

//Do nothing Dtor
Point::~Point() 
{
	//cout << "delet Point " << *this << endl;
};

//Set x and y
void Point::Set_data(float x, float y) { this->x = x; this->y = y; };

//Use operator overloading to get private member data
float Point::operator[](int i) const
{
	if (i == 0)
		return x;
	else if (i == 1)
		return y;
	else 
		return -1;
};

//operator overloading to cin point
istream& operator>>(istream& in, Point& p)
{
	in >> p.x >> p.y;
	return in;
};

//operato overloading to cout point
ostream & operator<<(ostream& out, const Point& p)
{
	out << fixed << setprecision(3) << "( " << p.x << " , " << p.y << " )\n";
	return out;
};

//Initial data and set nextNode is point to null
QuadtreeNode::QuadtreeNode(const Point& p1, const Point& p2, const float size):data(&p1),separate_point(p2),size(size)
{
	for (int i = 0; i < 4; ++i)
		nextNode[i] = NULL;
	//cout << "create new node " << p1;
};

//To avoid error which occur when Dtor, we need to malloc new dynamic memory space to store data
//and use same function to copy nextNode
QuadtreeNode::QuadtreeNode(const QuadtreeNode& node):size(node.size),separate_point(node.separate_point)
{
	if (node.data != NULL)
		data = new Point((*node.data)[0], (*node.data)[1]);
	else
	{
		data = NULL;
		for (int i = 0; i < 4; ++i)
			if (node.nextNode[i] != NULL)
			{
				nextNode[i] = new QuadtreeNode(*node.nextNode[i]);
			}
	}
};

//Remenber free date
QuadtreeNode::~QuadtreeNode()
{
	if(data != NULL)
		delete data;
	else
		for (int i = 0; i < 4; ++i)
		{
			if (nextNode[i] != NULL)
				delete nextNode[i];
		}	
};//#16


bool QuadtreeNode::insertPoint(const Point& p) 
{
	int quadrant = 0;
	//cout << "insert " << p;
	Point sep;

	//Determinde th quadrant of point and set separate_point
	if (p[0] > separate_point[0])
		if (p[1] > separate_point[1])
		{
			quadrant = 0;
			sep.Set_data(separate_point[0] + size / 2, separate_point[1] + size / 2);
		}
		else
		{
			quadrant = 3;
			sep.Set_data(separate_point[0] + size / 2, separate_point[1] - size / 2);
		}
	else
		if (p[1] > separate_point[1])
		{
			quadrant = 1;
			sep.Set_data(separate_point[0] - size / 2, separate_point[1] + size / 2);
		}
		else
		{
			quadrant = 2;
			sep.Set_data(separate_point[0] - size / 2, separate_point[1] - size / 2);
		}
	//cout << "quadrant is " << quadrant << endl;

	//if quadrant is NULL, need to malloc new node
	//if not use same function insert point in nextNode
	if (nextNode[quadrant] == NULL)
	{
		nextNode[quadrant] = new QuadtreeNode(p, sep, size / 2);
	}
	else
	{
		nextNode[quadrant]->insertPoint(p);
	}

	//if this data != null , need to distribute this point to nextNode
	if (data != NULL)
	{
		const Point* temp = data;
		data = NULL;
		insertPoint(*temp);
	}

	return true;
};//#17s

const Point* QuadtreeNode::FindClosestPoint(float x, float y)const
{
	//if data is null, need to find the quadrant of this point, and keep find
	if (data == NULL)
	{
		int quadrant = 0;
		if (x > separate_point[0])
		{
			if (y > separate_point[1])
				quadrant = 0;
			else 
				quadrant = 3;
		}
		else
		{
			if (y > separate_point[1])
				quadrant = 1;
			else 
				quadrant = 2;
		}

		if (nextNode[quadrant] != NULL)
			return nextNode[quadrant]->FindClosestPoint(x, y);
		else
		{
			cout << "此子節點中沒有相對應的點\n";
			return data;
		}
	}

	//if data != null, data is finial node
	return data;
};