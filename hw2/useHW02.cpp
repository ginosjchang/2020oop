﻿// useHW02.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
// E14062034 張少鈞 HW02

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "HW02.h"

using namespace std;

int main()
{
	//Open input data
	ifstream inFile("Point_HW2.txt", ios::in);

	//Check input data open correctly
	if (!inFile)
	{
		cerr << "can't find file\n";
		exit(1);
	}

	/*txt read inputfile
	  number get number of point*/
	string txt;
	int number;

	//Read first line
	getline(inFile, txt);

	//If first contain "Point count" ,get number of point
	//If not,exit program
	if (txt.find("Point count") != string::npos)
	{
		try
		{
			//convert string to int
			number = stoi(txt.substr(txt.find(":") + 1));
		}
		catch (exception e)
		{
			//if stoi throw exception, the convert is faill
			cerr << "error number of point\n";
			exit(2);
		}
		//cout << "input point number is " << number << endl;
	}
	else
	{
		cerr << "didn't define number of point\n";
		exit(3);
	}

	
	QuadtreeNode *node = NULL;

	//Insert Point
	for (int i = 0; i < number; ++i)
	{
		Point *p = new Point();
		inFile >> *p;
		//cout << *p;
		if (i == 0)
		{
			//Create first node
			node = new QuadtreeNode(*p, Point(0, 0), 100);
		}
		else
			node->insertPoint(*p);
	}

	//Get input point from user
	float x = 0,
		  y = 0;
	
	cout << "起輸入欲尋找點的x,y座標(直接輸入兩數字即可)\n";
	
	//Check input data type
	if (!(cin >> x) || !(cin >> y))
	{
		cerr << "error input\n";
		exit(4);
	}

	const Point* p = node->FindClosestPoint(x, y);
	if (p != NULL)
		cout << "此所屬子節點中的點為 : " << *p;

	//Test copy Ctor
	QuadtreeNode* copy_node = new QuadtreeNode(*node);

	cout << "複製的四元樹";
	p = copy_node->FindClosestPoint(x, y);
	if (p != NULL)
		cout << "所屬子節點的點為 : " << *p;
		
	//free dynamic memory allocation
	delete node;
	delete copy_node;


	cout << "\n以上兩類別中，有哪些成員函式可以直接使用編譯器預設的版本?為什麼?\n"
		<< "C++11明確規定編譯器會自動產生的預設函式有\n"
		<< "1.預設建構函式\n"
		<< "\t對於Point與QuadtreeNode而言兩者均有多個建構子，並且均有不同的功能，因此無法使用預設建構子\n"
		<< "2.複製建構函式\n"
		<< "\tQuadtreeNode的複製建構函式因為內部data資料採取new的方式來進行儲存，若使用預設複製建構函式則在其解構時將會產生記憶體錯誤\n"
		<< "3.複製指派運算子\n"
		<< "\t本次並未使用複製指派運算子因此不列入考慮\n"
		<<"4.解構子\n"	
		<< "\t對於QuadtreeNode內部有new產生的動態記憶體配置，因此在解構時要特別刪除，因此不可使用預設\n"
		<< "\t而Point內部均為靜態變數，不須刻意釋放記憶體空間，因此可以使用預設的解構子\n"
		<< "\n因此判斷只有#4可以使用編譯器預設的函式\n";

}

