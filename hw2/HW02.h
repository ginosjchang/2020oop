#pragma once
#ifndef HW02_H
#define HW02_H

#include <iostream>
#include <iomanip>

using namespace std;

class Point
{
private://#1
	float x;
	float y;

public:
	Point();//#2
	Point(float, float);//#3
	~Point();//#4

	void Set_data(float, float);//#5
	float operator[](int i)const;//#6
	friend istream& operator>>(istream&, Point &);//#7
	friend ostream& operator<<(ostream&, const Point&);//#8
};

class QuadtreeNode
{
private:
	const Point* data;//#9
	QuadtreeNode* nextNode[4];//#10
	const Point separate_point;//#11
	const float size;//#12
	QuadtreeNode();//#13

public:
	QuadtreeNode(const Point& ,const Point& ,const float);//#14
	QuadtreeNode(const QuadtreeNode&);//#15
	~QuadtreeNode();//#16

	bool insertPoint(const Point&);//#17
	const Point* FindClosestPoint(float,float)const;
};
#endif // !HW02_H

