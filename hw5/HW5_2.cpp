﻿// HW5_2.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

#include <iostream>
#include <initializer_list>
#include <vector>

template<typename T>
auto average_list(std::initializer_list<T> init_list)
{
	T* temp = new T[init_list.size()];
	int i = 0;
	for (auto item = init_list.begin(); item != init_list.end(); ++item,++i)
	{
		temp[i] = *item;
	}
	return temp;
}

int main()
{
	using namespace std;
	// list of double deduced from list contents
	auto q = average_list({ 15.4, 10.7, 9.0 });
	cout << q << endl;
	// list of int deduced from list contents
	cout << average_list({ 20, 30, 19, 17, 45, 38 }) << endl;
	// forced list of double
	auto ad = average_list<double>({ 'A', 70, 65.33 });
	cout << ad << endl;
	return 0;
}

