﻿// E14062034_HW5.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>

auto TooBig = [](int n) {return n > 100; };
auto outint = [](int n) { std::cout << n << " "; };

int main()
{
	auto f100 = TooBig;
	int vals[10] = { 50, 100, 90, 180, 60, 210, 415, 88, 188, 201 };
	std::list<int> yadayada(vals, vals + 10); // range constructor
	std::list<int> etcetera(vals, vals + 10);

	// C++0x can use the following instead
   //  list<int> yadayada = {50, 100, 90, 180, 60, 210, 415, 88, 188, 201};
   //  list<int> etcetera {50, 100, 90, 180, 60, 210, 415, 88, 188, 201};

	std::cout << "Original lists:\n";
	for_each(yadayada.begin(), yadayada.end(), outint);
	std::cout << std::endl;
	for_each(etcetera.begin(), etcetera.end(), outint);
	std::cout << std::endl;
	yadayada.remove_if(f100);               // use a named function object
	etcetera.remove_if([](int n) {return n > 200; });   // construct a function object
	std::cout << "Trimmed lists:\n";
	for_each(yadayada.begin(), yadayada.end(), [](int n) {std::cout << n << " "; });
	std::cout << std::endl;
	for_each(etcetera.begin(), etcetera.end(), [](int n) {std::cout << n << " "; });
	std::cout << std::endl;
	std::cin.get();
	return 0;
}
