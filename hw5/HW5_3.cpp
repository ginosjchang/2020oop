﻿// HW5_3.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

#include <fstream>
#include <iostream>
#include <iomanip>
#include <chrono>

using namespace std;

class Point
{
public:
	float x;
	float y;
	float z;

	Point() :x(0), y(0), z(0) {}

	bool operator ==(const Point p)
	{
		if (x == p.x && y == p.y && z == p.z)
			return true;
		return false;
	}

	Point& operator =(const Point p)
	{
		this->x = p.x;
		this->y = p.y;
		this->z = p.z;
		return *this;
	}
};

Point* LoadPoint(const char*, unsigned int&);

int main()
{
	const char filename[3][15] = { "HW5-3-1.bin","HW5-3-2.bin","HW5-3-3.bin" };
	std::chrono::steady_clock::time_point start, end;//紀錄時間的變數

	for (int file = 0; file < 3; ++file)
	{
		start = std::chrono::steady_clock::now();//紀錄讀檔開始時間
		unsigned int nPoint = 0;

		Point* point_array = LoadPoint(filename[file], nPoint);//讀點
		end = chrono::steady_clock::now();//紀錄讀檔結束時間
		cout << filename[file]<<"\n" << "Time " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms\n";
		cout << "Number of point " << nPoint << endl;

		start = std::chrono::steady_clock::now();//紀錄移除重複點開始時間
		//請將移除重複點演算法撰寫於此
		for (unsigned int i = 0; i < nPoint; ++i)
		{
			for (unsigned int j = i + 1; j < nPoint; ++j)
			{
				if (point_array[i] == point_array[j])
				{
					Point temp = point_array[j];
					point_array[j] = point_array[nPoint - 1];
					point_array[nPoint - 1] = temp;
					--nPoint;
					--j;
				}
			}
		}

		end = std::chrono::steady_clock::now();//紀錄移除重複點結束時間
		cout << "Remove Time " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms\n";
		cout << "Number of remained point " << nPoint << endl;

		delete[]point_array;
	}

	system("pause");
	return 0;
}


Point* LoadPoint(const char* filename, unsigned int& nPoint)		// 從文字檔讀入多個點資料
{
	using namespace std;
	Point* pPoint;

	try
	{
		if (!filename)
			throw new exception("引數錯誤");

		ifstream fin;
		fin.open(filename, ios::in | ios::binary);
		if (!fin.good())
			throw new exception("檔名或路徑錯誤! 無法開啟檔案!");
		const unsigned int tmpLen = 80;
		char tmpBuff[tmpLen];

		//read Header
		fin.read(tmpBuff, tmpLen);
		if (!fin.good())
			throw new exception("格式不合(header錯誤)");

		//how many point ?
		fin.read((char *)&nPoint, 4);//unsigned long, must be 4 bytes
		if (!fin.good())
			throw new exception("格式不合(三角網格數錯誤)");

		//allocate array memory
		if (nPoint == 0)
		{
			throw new exception("NO Point!");
		}

		pPoint = new Point[nPoint];

		//read triangles
		fin.seekg(84, ios::beg);
		for (unsigned int i = 0; i < nPoint; i++)
		{
			fin.read(tmpBuff, 14);
			if (!fin.good())
				throw new exception("格式不合");
			int gc = fin.gcount();
			if (gc < 14)
				throw;

			memcpy(&pPoint[i], tmpBuff, 12);
		}
		fin.clear();
		fin.close();
	}
	catch (exception &e)
	{
		cout << e.what() << "\n";
		pPoint = 0;
		return (Point *)0;
	}

	return pPoint;
}

