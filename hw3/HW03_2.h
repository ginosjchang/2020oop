#pragma once
#ifndef HW03_2_H
#define HW03_2_H

#include <iostream>
using std::cout;
using std::ostream;

template<class KeyType>
class Queue
{
	/*進行queue輸出的freid*/
	friend ostream& operator<<(ostream& out, Queue& q)
	{
		if (q.IsEmpty()) return out;
		int curr = (q.front + 1) % q.MaxSize,
			last = (q.rear + 1) % q.MaxSize;
		do
		{
			cout << q.queue[curr] << " ";
			curr = (++curr) % q.MaxSize;
		} while (curr != last);
		return out;
	}
private:
	int front, rear;
	KeyType* queue;
	int MaxSize;
public:
	Queue(int MaxQueueSize = 20)
	{
		if/*邏輯運算子if*/ (/*邏輯運算子判斷條件的右小括號*/MaxQueueSize < 0/*邏輯運算子判斷條件*/)/*邏輯運算子判斷條件的左小括號*/ MaxQueueSize = 0/*滿足邏輯運算條件要執行的項目*/;
		front = 0;/*初始化front的數值*/
		rear = 0;/*初始化rear的數值*/
		MaxSize = MaxQueueSize + 1;/*將一維矩陣的大小設定為欲放置數量加一 如果不懂可以去修資料結構*/
		queue = new KeyType[MaxSize];/*這邊是在建立動態記憶體配置 MaxSize是配製的空間數喔*/
	}
	Queue(Queue& q)
	{
		front = q.front;/*複製front*/
		rear = q.rear;/*複製rear*/
		MaxSize = q.MaxSize;/*複製MaxSize*/
		queue = new KeyType[MaxSize];/*建立跟欲複製的queue一樣的大小*/
		for/*迴圈敘述for*/ (/*for迴圈敘述的右小括號*/int i = 0/*迴圈的初始變數初始化*/;/*分開初始變數與判斷式的分號*/ i < q.MaxSize/*判斷式 如為滿足則會跳出迴圈喔*/;/*分開判斷式與遞增式的分號*/ ++i/*++i = i + 1 把變數i加一*/)/*for迴圈敘述的右小括號*/
			queue[i] = q.queue[i];/*把每個元素複製過來 使用的class要記得寫指定運算子喔 不要因為沒寫扣我分*/
	}
	~Queue()
	{
		del();/*把動態記憶體配置的空間刪掉 不然很浪費空間*/
	}
	bool IsFull()
	{
		return (rear + 1) % MaxSize == front;/*判斷rear可不可以往後移 如果不懂真的可以去修資訊系的資料結構*/
	}
	bool IsEmpty()
	{
		return front == rear;/*判斷rear跟front一樣不樣 資料結構是禮拜四的7~9節*/
	}
	void Add(const KeyType& item)
	{
		if (IsFull())
		{
			QueueFull();/*呼叫一個我不知道要幹嘛 只好用來輸出提醒queue滿的函式*/
			return;/*到這邊就會跳出function不會繼續執行*/
		}
		rear = (rear + 1) % MaxSize;/*把rear往後移一個準備把東西放進去*/
		queue[rear] = item;/*把東西放進去*/
	}
	KeyType* Delete()
	{
		if (IsEmpty())
		{
			QueueEmpty();/*右是呼叫一個不知道要幹嘛 只好用來輸出提醒queue是空的函式*/
			return 0;/*回傳*/
		}

		front = (front + 1) % MaxSize;/*把fornt往後移準備把東西移出去*/
		KeyType* temp = new KeyType(queue[front]);/*建立新空間並複製要傳出去的東西 如果不建立新空間可能會被直接蓋掉資料就不見了*/
		/*記得要確認class的複製建構子喔*/
		return temp;/*回傳指標*/
	}
	Queue& operator=(Queue q)
	{
		del();/*把動態記憶體配置的空間刪掉 不然很浪費空間*/
		front = q.front;/*初始化front的數值*/
		rear = q.rear;/*初始化rear的數值*/
		MaxSize = q.MaxSize;/*初始化MaxSize的數值*/
		queue = new KeyType[MaxSize];/*建立跟欲複製的queue一樣的大小*/
		/*這邊我懶得再打一次了*/
		for (int i = 0; i < q.MaxSize; ++i)
			queue[i] = q.queue[i];
	}
private:
	void del()
	{
		/*確定好刪除的方式
		delete 跟 delete[]很不一樣*/
		if (MaxSize == 1)
			delete queue;
		else if (MaxSize > 1)
			delete[] queue;
	}
	void QueueFull() { cout << " Queue is full"; }
	void QueueEmpty() { cout << " Queue is empty"; }
};
#endif // !HW03_2_H

