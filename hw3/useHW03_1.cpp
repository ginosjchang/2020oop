﻿//E14062034 HW03_1
#include <iostream>
#include "HW03_1.h"

using namespace std;


int main()
{
	IGeometry** arr = new IGeometry*[3];
	arr[0] = new Trapezoid();
	arr[1] = new Prism();
	arr[2] = new Cone();
	for (int i = 0; i < 3; ++i)
	{
		switch (i)
		{
		case 0:cout << "\nTrapezoid :\n" << dynamic_cast<Trapezoid&>(*arr[i]); break;
		case 1:cout << "\nPrism :\n" << dynamic_cast<Prism&>(*arr[i]); break;
		case 2:cout << "\nCone :\n" << dynamic_cast<Cone&>(*arr[i]); break;
		}
		/*Copy constructor and assignment operator*/
		arr[i]->testCopyAss();
		/*virtal overriding*/
		arr[i]->showIGeometry();
		/*show each spacial function*/
		arr[i]->showFunction();
		cout << endl;
	}
	for (int i = 0; i < 3; ++i)
		delete arr[i];
	delete[] arr;
	return 0;
}