#pragma once
#ifndef HW03_1_H
#define HW03_1_H

#define PI 3.14159265359

#include <stdlib.h>
#include <ctime>
#include <cmath>
#include <iostream>

using std::cout;
using std::cin;
using std::ostream;
using std::istream;

static void setSRand() { srand(time(NULL)); }
static double randNext() { return (rand() % 100 + 1) / 10.0; };

class Point
{
	friend ostream& operator <<(ostream&, const Point&);
public:
	double x, y, z;

	Point(double,double,double);
	Point& operator=(const Point);
	Point operator+(const Point) const;
	Point& operator+=(const Point);
	Point operator*(const double) const;
	Point& operator*=(const double);
	double distance(const Point) const;
private:
};

class IGeometry
{
public:
	void showIGeometry();//show Area Perimeter Volume
	virtual void showFunction();//show other function in child
	virtual void testCopyAss();//Use Copy Constructor and assignment operator and show result
	virtual double Area();
	virtual double Perimeter();
	virtual double Volume();
};

class Trapezoid : public IGeometry
{
	friend ostream& operator <<(ostream&, const Trapezoid&);
public:
	Trapezoid();
	Trapezoid(Trapezoid&);//new to new memory location for vertices
	~Trapezoid();//release memory
	/*Overriding*/
	virtual void showFunction();
	virtual void testCopyAss();
	virtual double Area();
	virtual double Perimeter();
	virtual double Volume();
	/*Other function*/
	double* SideLength();
	double* SideArea();
	/*Operator overloading*/
	Trapezoid& operator=(Trapezoid);
private:
	/*first 4 point for underside*/
	Point* vertices = nullptr;

	double getHeight();//return the height of Trapezoid
};

class Prism : public IGeometry
{
	friend ostream& operator <<(ostream&, const Prism&);
public:
	Prism();
	/*Overriding*/
	virtual void showFunction();
	virtual void testCopyAss();
	virtual double Area();
	virtual double Perimeter();
	virtual double Volume();
	/*Other function*/
	double Height();
	double BottomArea();
	double SideArea();
	/*Operator overloading*/
	Prism& operator=(Prism);
private:
	Point Top, Bottom;
	double r;
};

class Cone : public IGeometry
{
	friend ostream& operator<<(ostream&, const Cone&);
public:
	Cone();
	/*Overriding*/
	virtual void showFunction();
	virtual void testCopyAss();
	virtual double Area();
	virtual double Perimeter();
	virtual double Volume();
	/*Other function*/
	double Height();
	double Angle();
	/*Operator overloading*/
	Cone& operator=(Cone);
private:
	Point Top, Center;
	double r;

	double BottomArea();//Count Bottom Area
};

#endif // !HW03_1_H

