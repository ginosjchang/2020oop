﻿// useHW03_2.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

//E14062034 HW03_2
#include <iostream>
#include "HW03_2.h"

using namespace std;

int main()
{
	int size = 5;
	Queue<int> queue(5);
	cout << "Create a Queue to store int\nQueue size is " << size << endl;
	/*輸入測試資料*/
	for (int i = 0; i < 6; ++i)
	{
		cout << "\nInput " << i;
		queue.Add(i);
	}
	/*測試複製建構子與指定運算子*/
	Queue<int> copy(queue);
	Queue<int> ass = queue;
	cout << "\ncurrent queue : " << queue
		<< "\nCopy Constructor queue : " << copy
		<< "\nAssignment operator : " << ass;
	int *output;
	/*提取資料*/
	for (int i = 0; i < 6; ++i)
	{
		cout << "\nDelete : ";
		output = queue.Delete();
		cout << "get " << output;
		if (output != 0) delete output;
	}
	return 0;
}
