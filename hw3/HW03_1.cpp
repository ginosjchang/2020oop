#include "HW03_1.h"
/***Point***/
ostream& operator <<(ostream& out, const Point& p)
{
	out << " (" << p.x << " , " << p.y << " , " << p.z << " )";
	return out;
}
Point operator*(double x, Point p)
{
	return p * x;
}
Point::Point(double x = 0, double y = 0, double z = 0):x(x),y(y),z(z){}
Point& Point::operator=(const Point p)
{
	x = p.x;
	y = p.y;
	z = p.z;
	return *this;
}
Point Point::operator+(const Point p) const
{
	Point temp(*this);
	temp.x += p.x;
	temp.y += p.y;
	temp.z += p.z;
	return temp;
}
Point& Point::operator+=(const Point p)
{
	this->x += p.x;
	this->y += p.y;
	this->z += p.z;
	return *this;
}
Point Point::operator*(const double a) const
{
	Point temp(*this);
	temp.x *= a;
	temp.y *= a;
	temp.z *= a;
	return temp;
}
Point& Point::operator*=(const double a)
{
	this->x *= a;
	this->y *= a;
	this->z *= a;
	return *this;
}
double Point::distance(const Point p) const
{
	return sqrt(pow(p.x - this->x, 2) + pow(p.y - this->y, 2) + pow(p.z - this->z, 2));
}

/***IGeometry***/
void IGeometry::showIGeometry()
{
	cout << "\nArea : " << Area()
		<< "\nPerimeter : " << Perimeter()
		<< "\nVolume : " << Volume();
}
void IGeometry::showFunction(){}
void IGeometry::testCopyAss(){}
double IGeometry::Area() { return 0; }
double IGeometry::Perimeter(){ return 0; }
double IGeometry::Volume(){ return 0; }

/***Trapezoid***/
ostream& operator <<(ostream& out, const Trapezoid& t)
{
	for (int i = 0; i < 8; ++i)
	{
		out << t.vertices[i] << " ";
	}
	return out;
}
Trapezoid::Trapezoid()
{
	/*
	Underside
	3 2
	0 1
	Uperside
	7 6
	4 5
	*/
	vertices = new Point[8];

	//random the length of square and height,which is between 1~10.
	double size_lower = randNext(),
		   size_upper = randNext(),
		   height = randNext();
	Point vx = Point(1, 0, 0),
		  vy = Point(0, 1, 0),
		  vz = Point(0, 0, 1);
	vertices[1] = vx*size_lower;
	vertices[3] = vy*size_lower;
	vertices[2] = vertices[1] + vy*size_lower;
	vertices[4] = vx * ((size_lower - size_upper) / 2) + vy * ((size_lower - size_upper) / 2) + vz * height;
	vertices[5] = vertices[4] + vx * size_upper;
	vertices[7] = vertices[4] + vy * size_upper;
	vertices[6] = vertices[5] + vy * size_upper;
}
Trapezoid::Trapezoid(Trapezoid& t)
{
	vertices = new Point[8];
	for (int i = 0; i < 8; ++i)
	{
		vertices[i] = t.vertices[i];
	}

}
Trapezoid::~Trapezoid()
{
	if (vertices != nullptr)
		delete[] vertices;
}
void Trapezoid::showFunction()
{
	double *d[2];
	d[0] = SideLength();
	d[1] = SideArea();
	cout << "\nSideLength : " << d[0][0] << " , " << d[0][1] << " , " << d[0][2]
		<< "\nSideArea : " << d[1][0] << " , " << d[1][1] << " , " << d[1][2];
	delete[] d[0];
	delete[] d[1];
}
void Trapezoid::testCopyAss() 
{
	Trapezoid copy(*this);
	Trapezoid ass = *this;
	cout << "\nCopy Constructor :\n" << copy
		<< "\nAssignment operator :\n" << ass;
}
double Trapezoid::Area()
{
	double *area = SideArea();
	double a = area[0] + area[1] + area[2] * 4;
	delete[] area;
	return a;
}
double Trapezoid::Perimeter()
{
	double *len = SideLength();
	double l = len[0] + len[1] + len[2];
	delete[] len;
	return l * 4;
}
double Trapezoid::Volume()
{
	double *len = SideLength();
	double l[2] = { len[0],len[1] };
	delete[] len;
	return (l[0] * l[0] + l[1] * l[1] + pow(l[0] + l[1], 2))*getHeight() / 6;
}
double* Trapezoid::SideLength()
{
	double* len = new double[3];
	len[0] = vertices[0].distance(vertices[1]);
	len[1] = vertices[4].distance(vertices[5]);
	len[2] = vertices[0].distance(vertices[4]);
	return len;
}
double* Trapezoid::SideArea()
{
	double *area = new double[3];
	area[0] = pow(vertices[0].distance(vertices[1]), 2);
	area[1] = pow(vertices[4].distance(vertices[5]), 2);
	Point temp(vertices[0]);
	temp.x = vertices[4].x;
	area[2] = (vertices[0].distance(vertices[1]) + vertices[4].distance(vertices[5]))*vertices[4].distance(temp) / 2;
	return area;
}
Trapezoid& Trapezoid::operator=(Trapezoid t)
{
	for (int i = 0; i < 8; ++i)
		vertices[i] = t.vertices[i];
	return *this;
}
double Trapezoid::getHeight()
{
	return vertices[4].z - vertices[0].z;
}

/***Prism***/
ostream& operator <<(ostream& out, const Prism& p)
{
	out << "Top : " << p.Top
		<< "\nBottom : " << p.Bottom
		<< "\nlength of side : " << p.r;
	return out;
}
Prism::Prism()
{
	srand(time(NULL));
	Bottom.x = randNext();
	Bottom.y = randNext();
	Top.x = Bottom.x;
	Top.y = Bottom.y;
	Top.z = randNext();
	r = randNext();
}
void Prism::showFunction()
{
	cout << "\nHeight : " << Height()
		<< "\nBottomArea : " << BottomArea()
		<< "\nSideArea : " << SideArea();
}
void Prism::testCopyAss() 
{
	Prism copy(*this);
	Prism ass = *this;
	cout << "\nCopy Constructor :\n" << copy
		<< "\nAssignment operator :\n" << ass;
}
double Prism::Area()
{
	return BottomArea() * 2 + SideArea() * 3;
}
double Prism::Perimeter()
{
	return r * 6 + Height() * 3;
}
double Prism::Volume()
{
	return BottomArea()*Height();
}
double Prism::Height()
{
	return Top.z - Bottom.z;
}
double Prism::BottomArea()
{
	return sqrt(3)*r / 2.0;
}
double Prism::SideArea()
{
	return r * Height();
}
Prism& Prism::operator=(Prism p)
{
	Bottom = p.Bottom;
	Top = p.Top;
	r = p.r;
	return *this;
}

/***Cone***/
ostream& operator<<(ostream& out, const Cone& c)
{
	out << "Top : " << c.Top
		<< "\nCenter : " << c.Center
		<< "radius : " << c.r;
	return out;
}
Cone::Cone()
{
	Center.x = randNext();
	Center.y = randNext();
	Top = Center;
	Top.z = randNext();
	r = randNext();
}
void Cone::showFunction()
{
	cout << "\nHeight : " << Height()
		<< "\nAngle : " << Angle();
}
void Cone::testCopyAss() 
{
	Cone copy(*this);
	Cone ass = *this;
	cout << "\nCopy Constructor :\n" << copy
		<< "\nAssignment operator :\n" << ass;
}
double Cone::Area()
{
	return PI * r * (r + sqrt(r*r + pow(Height(), 2)));
}
double Cone::Perimeter()
{
	return 2 * PI * r + 2 * sqrt(r*r + pow(Height(), 2));
}
double Cone::Volume()
{
	return BottomArea() * Height() / 3;
}
double Cone::Height() { return Top.z - Center.z; }
double Cone::Angle()
{
	return r * 360.0 / sqrt(r*r + pow(Height(), 2));
}
Cone& Cone::operator=(Cone c)
{
	Center = c.Center;
	Top = c.Top;
	r = c.r;
	return *this;
}
double Cone::BottomArea()
{
	return r * r * PI;
}